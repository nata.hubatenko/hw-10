window.onload = function () {
	document.querySelector('.tabs').addEventListener('click', getText);
}

function getText(event) {
	let dataTab = event.target.getAttribute('data-tab');
	let tabsTitle = document.getElementsByClassName('tabs-title');
	for (let i = 0; i < tabsTitle.length; i++) {
		tabsTitle[i].classList.remove('active');
	}
	event.target.classList.add('active');
	let tabsContent = document.getElementsByClassName('tabs-text');
	for (let i = 0; i < tabsContent.length; i++) {
		if (dataTab == i) {
			tabsContent[i].style.display = 'block';
		} else {
			tabsContent[i].style.display = 'none';
		}
	}
}
